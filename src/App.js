import React from 'react';
import {Switch, Route} from 'react-router-dom';
import Layout from "./components/Layout/Layout";
import MainScreen from "./containers/MainScreen/MainScreen";
import Edit from "./containers/Edit/Edit";


const App = () => (
    <>
        <Layout>
            <Switch>
                <Route path='/editPage' exact component={Edit}/>
                <Route path='/:name' exact component={MainScreen}/>
            </Switch>
        </Layout>
    </>
);

export default App;
