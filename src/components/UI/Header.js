import React from 'react';
import {NavLink} from "react-router-dom";

const Header = () => {
    return (
        <div className={'nav'}>
            <h2 className="name">Static Pages</h2>
            <div className="main-item">
                <div className={'item'}>
                    <NavLink className="link" to={'/home'}>Home</NavLink>
                </div>
                <div className={'item'}>
                    <NavLink className="link" to={'/about'}>About</NavLink>
                </div>
                <div className={'item'}>
                    <NavLink className="link" to={'/contacts'}>Contacts</NavLink>
                </div>
                <div className={'item'}>
                    <NavLink className="link" to={'/divisions'}>Divisions</NavLink>
                </div>
                <div className={'item'}>
                    <NavLink className="link" to={'/editPage'}>Edit</NavLink>
                </div>
            </div>

        </div>
    );
};

export default Header;