import React from 'react';

import './Layout.css'
import Header from "../UI/Header";
const Layout = (props) => {
    return (
        <main>
            <Header/>
            {props.children}
        </main>
    );
};

export default Layout;