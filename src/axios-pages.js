import axios from "axios";

const axiosPages = axios.create({
    baseURL: 'https://static-pages-erzhan-default-rtdb.firebaseio.com/'
});

export default axiosPages;
