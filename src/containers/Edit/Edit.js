import React, {useState, useEffect} from 'react';
import axiosPages from "../../axios-pages";
import './Edit.css';

const Edit = props => {

    const [edit, setEdit] = useState({});
    const [listPages, setListPages] = useState([]);
    const [page, setPage] = useState('about');

    useEffect(() => {
        const getPages = async () => {
            try {
                const response = await axiosPages.get('/pages.json');
                const pages = Object.keys(response.data).map(key => {
                    return key;
                })
                setListPages(pages)
            } catch(e) {
                console.log(e);
            }
        }
        getPages()
    }, [])

    useEffect(() => {
        const getData = async () => {
            try {
                const response = await axiosPages.get('pages/' + page + '.json');
                setEdit(response.data);
            } catch (e) {
                console.log(e);
            }
        }
        getData();
    }, [page]);

    const onChange = (e) => {
        const {name, value} = e.target;
        setEdit(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const editSave = async () => {
        const data = {
            title: edit.title,
            text: edit.text,
        }

         try {
             const response = axiosPages.put(`pages/${page}.json`, data);
             setEdit(response.data);
             props.history.push('/');
         } catch (e) {
             console.log(e);
         }
    };

    const changeSelect = (e) => {
        setPage(e.target.value);
    };

    return (
        <div className="Edit">
            <select onChange={changeSelect} name="pageName" id="pageName">
                {listPages.map(item => {
                    return (
                        <option key={item} value={item}>{item}</option>
                    )
                })}
            </select>

            <input
                type="text"
                value={edit.title}
                name="title"
                onChange={(e) => onChange(e)}
            />
            <textarea
                cols="30"
                rows="10"
                value={edit.text}
                name="text"
                onChange={(e) => onChange(e)}
            />

            <button className="button" onClick={editSave}>
                <span>Save</span>
            </button>
        </div>
    );
};

export default Edit;