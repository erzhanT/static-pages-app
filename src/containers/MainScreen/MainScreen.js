import React, {useState, useEffect} from 'react';
import axiosPages from "../../axios-pages";
import './MainScreen.css';

const MainScreen = (props) => {
   const name = props.match.params.name
    const [data, setData] = useState({});

    useEffect(() => {
        const getData = async () => {
            try {
                const response = await axiosPages.get('pages/' + name + '.json');
                setData(response.data);
            } catch (e) {
                console.log(e);
            }
        }
        getData();
    }, [name]);

    return (
        <>
            {data && <div className='box'>
                <h3>{data.title}</h3>
                <p>{data.text}</p>
            </div>}
        </>
    );
};

export default MainScreen;